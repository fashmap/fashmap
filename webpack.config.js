const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const config = {
  entry: './src/web/map.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    publicPath: './',
    filename: '[name].js',
  },
  plugins: [
    new HtmlWebpackPlugin({ template: 'src/web/index.html' }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'defer',
    }),
    new CopyPlugin([
      { from: './src/web/*.css', to: 'index.css' },
      { from: './node_modules/leaflet/dist/leaflet.css', to: 'leaflet.css' },
      { from: './node_modules/leaflet/dist/images/', to: 'images/' },
    ]),
  ],
}

module.exports = config
