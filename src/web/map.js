import L from 'leaflet'
import members from '../../data/members.json'

const mapEl = document.querySelector('#map')
const map = L.map(mapEl).setView([34, 6]).setZoom(2)

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
}).addTo(map)

const layerGroup = L.layerGroup().addTo(map)

members
  .forEach((member) => {
    const marker = L.marker(member.loc.ll)
    const popup = L.popup().setContent(`email: ${member.email}, username: ${member.name}`)
    marker.bindPopup(popup)
    marker.addTo(layerGroup)
  })

// map.fitBounds(points))
