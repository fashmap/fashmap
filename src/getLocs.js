const geoip = require('geoip-lite')
const parse = require('csv-parse/lib/sync')
const fs = require('fs')
const R = require('ramda')

const members = fs.readFileSync('./data/core_members.csv')

const interestingFields = [
  'email',
  'name',
  'member_id',
  'ip_address',
  'pp_main_photo',
  'timezone',
  'signature',
  'member_title',
  'member_posts',
  'pp_reputation_points',
]

const processRecords = R.pipe(
  R.project(interestingFields),
  R.map((obj) => R.assoc('loc', geoip.lookup(obj.ip_address), obj)),
)

const parsedData = parse(members, {
  columns: true,
  skip_empty_lines: true,
})

const records = processRecords(parsedData)

fs.writeFileSync('./data/members.json', JSON.stringify(records, null, 2))

console.dir(records, { depth: 100 })
console.log(records.length)
